(function(){
    var optionMap = ['A','B',
        'C','D','E','F','G',
        'H','I','J','K','L',
        'M','N','O','P','Q',
        'R','S','T','U','V',
        'W','X','Y','Z'];
    /**
     * 用户选择选项
     */
    function optionSelect() {
        var selected = $(this).data("selected");
        if(selected === true)
            $(this).removeClass("selected");
        else
            $(this).addClass("selected");
        $(this).data("selected", !selected);

    }

    /**
     * 用户点击确定按钮
     */
    function confirmButtonClick() {
        // 取消选择
        var parent = $(this).parents(".question");
        var data = parent.data("questionData");
        var liDom = parent.find(".list li");
        var nextButton = parent.find(".next-button");
        liDom.removeClass("selected");
        liDom.off("click");

        // 隐藏确定按钮，显示下一步按钮
        $(this).addClass("question-hidden");
        if(data.curIndex < data.maxIndex) {
            nextButton.removeClass("question-hidden");
        }


        // 显示正确答案
        var options = data.questions[data.curIndex].option;

        liDom.each(function(i){
            if(options[i].right === true)
                $(this).addClass("correct");
        });
    }

    /**
     * 用户点击下一步按钮
     */
    function nextButtonClick() {
        var parent = $(this).parents(".question");
        var data = parent.data("questionData");
        ++data.curIndex;
        parent.data("questionData",data);
        refreshQuestion(parent);

    }

    /**
     * 设置问题
     * @param obj       对象
     * @param title     题目
     * @param option    答案选项
     */
    function refreshQuestion(obj) {
        var data = obj.data("questionData");
        var question = data.questions[data.curIndex];
        var title = question.title;
        var option = question.option;



        // 清空
        obj.empty();

        // 创建标题
        var titleDom = $("<p class='title'></p>");
        titleDom.text(title);
        obj.append(titleDom);

        // 创建ul选项
        var ulDom = $("<ul class='list'></ul>");
        ulDom.appendTo(obj);
        for(var i = 0 ; i < option.length ; ++i) {
            var liDom = $("<li></li>");

            // 选项
            var optionDom = $("<span class='option'></span>");
            optionDom.text(optionMap[i] + ".");

            // 答案描述
            var descDom = $("<span class='desc'></span>");
            descDom.text(option[i].desc);

            liDom.append(optionDom);
            liDom.append(descDom);
            liDom.appendTo(ulDom);

            liDom.data("selected", false);
            liDom.on("click",optionSelect);
        }

        // 创建按钮
        var buttonUlDom = $("<ul class='button-list'></ul>");
        var confirmDom = $("<button class=\"confirm-button\">确定</button>");
        var nextDom = $("<button class=\"next-button question-hidden\">下一题</button>");
        confirmDom.on("click",confirmButtonClick);
        nextDom.on("click",nextButtonClick);

        var liDom = $("<li></li>");
        buttonUlDom.append(liDom);
        liDom.append(confirmDom);

        liDom = $("<li></li>");
        buttonUlDom.append(liDom);
        liDom.append(nextDom);

        obj.append(buttonUlDom);
    }

    $.fn.question = function(questions) {
        // 初始化
        var data = {
            curIndex: 0,
            maxIndex: questions.length - 1,
            questions: questions
        };
        this.data("questionData", data);

        refreshQuestion(this);
    }

})();
